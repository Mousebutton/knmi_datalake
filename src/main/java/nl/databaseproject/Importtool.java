package nl.databaseproject;

import javax.sound.midi.Soundbank;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Importtool {
    public static void main(String args[]) throws URISyntaxException, IOException, SQLException {
//        Connection db = DatabaseConnection.getInstance();
//
//        long startTime = System.nanoTime();
//        List<WeatherData> weatherDataByJaar = new ArrayList<>();
//        for (int station : getAllStations()) {
//            weatherDataByJaar.addAll(WeatherDateRepository.getWeatherDataByStation(station));
//        }
//        long endTime = System.nanoTime();
//
//        long duration = (endTime - startTime);
//        System.out.println("Aantal resultaten: "+weatherDataByJaar.size());
//        System.out.println("Duur: "+duration/1000000+"ms");
//
//        for (WeatherData weatherData : weatherDataByJaar) {
//            System.out.println(weatherData);
//        }

        installScript();
    }
    private static void installScript() throws IOException, URISyntaxException, SQLException {
        createStationTable();
        insertStations();
        createDataTables();
        insertData();
    }
    private static void createDataTables() throws IOException, URISyntaxException {
        System.out.println("Creating data tables");

        getLines()
                .filter(line -> !line.startsWith("#"))
                .filter(line -> !line.split(",")[7].trim().isEmpty())
                .map(line -> line.split(",")[0].trim() + "-" + line.split(",")[1].substring(0, 4))
                .distinct()
                .forEach(Importtool::createTable);
        System.out.println("Created data tables");
    }

    private static void insertData() throws SQLException, IOException, URISyntaxException {
        System.out.println("Started creating insert queries");

        Connection db = DatabaseConnection.getInstance();
        List<Integer> allStations = getAllStations();

        Map<String, Map<String, List<String>>> insertDataMap = new HashMap<>();
        allStations.forEach(i -> insertDataMap.put(String.valueOf(i), new HashMap<>()));

        getLines()
                .filter(line -> !line.startsWith("#"))
                .filter(line -> !line.split(",")[7].trim().isEmpty())
                .map(line -> {
                    String[] split = line.split(",");
                    return new String[]{
                            split[0].trim(),
                            split[1].substring(0, 4),
                            split[1].substring(4, 6),
                            split[1].substring(6, 8),
                            split[2].trim(),
                            split[7].trim()
                    };
                })
                .forEach(line -> {
                    StringBuilder sb = new StringBuilder();
                    sb.append("(");
                    sb.append(line[2]);
                    sb.append(",");
                    sb.append(line[3]);
                    sb.append(",");
                    sb.append(line[4]);
                    sb.append(",");
                    sb.append(line[5]);
                    sb.append(")");
                    if (!insertDataMap.get(line[0]).containsKey(line[1])) {
                        insertDataMap.get(line[0]).put(line[1], new ArrayList<>());
                    }
                    insertDataMap.get(line[0]).get(line[1]).add(sb.toString());
                });
        System.out.println("Ended creating insert queries");

        insertDataMap.entrySet().parallelStream().forEach((entry) -> {
            String station = entry.getKey();
            Map<String, List<String>> yearColl = entry.getValue();

            Statement statement = null;
            try {
                statement = db.createStatement();
                db.setAutoCommit(false);
            } catch (SQLException e) {
                e.printStackTrace();
            }

            Statement finalStatement = statement;

            yearColl.forEach((key, value) -> {

                String tableName = station + "-" + key;
                String insertData = "insert into [" + tableName + "] values ";
                int count = 0;
                StringBuilder sb = new StringBuilder();
                System.out.println("Starting insert [" + tableName+"]");
                for (String s : value) {
                    count++;

                    sb.append(s);
                    sb.append(",");

                    if (count == 1000 || value.get(value.size() - 1).equals(s)) {
                        sb.deleteCharAt(sb.lastIndexOf(","));

                        try {
                            finalStatement.addBatch(insertData + sb.toString());
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }

                        count = 0;
                        sb = new StringBuilder();
                    }
                }
                try {
                    System.out.println("Started executing batch station data tables: " + station);
                    finalStatement.executeBatch();
                    db.commit();
                    finalStatement.clearBatch();
                    System.out.println("Ended executing batch station data tables: " + station);

                } catch (SQLException e) {
                    e.printStackTrace();
                }
                System.out.println("Ended insert [" + tableName+"]");
            });

        });
    }

    private static void insertStations() throws IOException, URISyntaxException {
        System.out.println("Inserting Stations");

        getLines()
                .filter(line -> !line.startsWith("#"))
                .filter(line -> !line.split(",")[7].trim().isEmpty())
                .map(line -> line.split(",")[0].trim())
                .distinct()
                .forEach(Importtool::insertStation);

        System.out.println("Inserted Stations");
    }

    private static void createStationTable() throws SQLException {
        System.out.println("Creating Station table");

        Connection db = DatabaseConnection.getInstance();
        String createTable = "IF OBJECT_ID('dbo.Station', 'U') IS NOT NULL \n" +
                "  DROP TABLE dbo.Station; \n" +
                "\n" +
                "CREATE TABLE [dbo].[Station](\n" +
                "\t[id] [int] NOT NULL,\n" +
                " primary key (id)\n" +
                " )";

        Statement statement = db.createStatement();
        statement.execute(createTable);

        System.out.println("Created Station table");
    }

    private static Stream<String> getLines() throws IOException, URISyntaxException {
        return Files.lines(Paths.get(Importtool.class.getClassLoader().getResource("KNMI_20171114_hourly.txt").toURI()));
    }

    public static void truncateTables() throws SQLException {
        Connection db = DatabaseConnection.getInstance();
        Statement statement = db.createStatement();
        for (String tableName : getAllTables()) {
            String query = "truncate table [" + tableName + "]";
            statement.execute(query);
        }
    }

    public static List<Integer> getAllStations() throws SQLException {
        Connection db = DatabaseConnection.getInstance();
        Statement statement = db.createStatement();
        ResultSet resultSet = statement.executeQuery("select * from Station");

        List<Integer> stations = new ArrayList<>();

        while (resultSet.next()) {
            stations.add(resultSet.getInt(1));
        }
        return stations;
    }

    private static List<Integer> getAllYears() throws SQLException {
        Connection db = DatabaseConnection.getInstance();
        String query = "SELECT distinct SUBSTRING(TABLE_NAME,5,4) as year FROM KNMI.INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' and TABLE_NAME like '%-%' order by year";
        Statement statement = db.createStatement();
        ResultSet resultSet = statement.executeQuery(query);

        List<Integer> tables = new ArrayList<>();
        while (resultSet.next()) {
            tables.add(resultSet.getInt(1));
        }
        return tables;
    }

    public static List<String> getAllTables() throws SQLException {
        Connection db = DatabaseConnection.getInstance();
        String query = "SELECT TABLE_NAME FROM KNMI.INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE'";
        Statement statement = db.createStatement();
        ResultSet resultSet = statement.executeQuery(query);

        List<String> tables = new ArrayList<>();
        while (resultSet.next()) {
            tables.add(resultSet.getString(0));
        }
        return tables;
    }

    public static void createTable(String tableName1) {
        String tableName = "create table [" + tableName1 + "]";

        String properties = " (" +
                "Maand tinyint not null," +
                "Dag tinyint not null," +
                "Uur tinyint not null," +
                "Temperatuur smallint not null," +
                "primary key (Maand,Dag,Uur)) ";

        String createTable = tableName + properties;

        try {
            Connection db = DatabaseConnection.getInstance();

            Statement statement = db.createStatement();
            statement.execute(createTable);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void insertStation(String id) {
        String insertStation = "insert into Station values (?)";
        try {
            Connection db = DatabaseConnection.getInstance();

            PreparedStatement statement = db.prepareStatement(insertStation);
            statement.setInt(1, Integer.valueOf(id));

            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

