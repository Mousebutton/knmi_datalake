package nl.databaseproject;

import lombok.*;

@Getter
@Setter
@Builder
@ToString
public class WeatherData {
    private int min;
    private int max;
    private int avg;

    private int station;
    private int jaar;
}
