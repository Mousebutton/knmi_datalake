package nl.databaseproject;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class WeatherDateRepository {
    private static Connection db;

    static {
        try {
            db = DatabaseConnection.getInstance();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public static List<WeatherData> getWeatherDataByYear(int stationId, int jaar) throws SQLException {
        String s = "select min(Temperatuur),max(Temperatuur),avg(Temperatuur) from [" + stationId + "-" + jaar + "]";
        Statement statement = db.createStatement();
        ResultSet resultSet = statement.executeQuery(s);

        List<WeatherData> weatherData = new ArrayList<>();
        while (resultSet.next()) {
            WeatherData w = WeatherData.builder()
                    .station(stationId)
                    .jaar(jaar)
                    .min(resultSet.getInt(1))
                    .max(resultSet.getInt(2))
                    .avg(resultSet.getInt(3))
                    .build();

            weatherData.add(w);
        }

        return weatherData;
    }

    public static List<WeatherData> getWeatherDataByStation(int stationId) throws SQLException {
        List<String> tableNames = getTableNameByStation(stationId);

        StringBuilder sb = new StringBuilder();
        sb.append("select min(result.min),max(result.max),avg(result.avg) from (");
        for (String tableName : tableNames) {
            sb.append("select min(Temperatuur) as min,max(Temperatuur) as max,avg(Temperatuur) as avg from [");
            sb.append(tableName);
            sb.append("]");
            if (!tableName.equals(tableNames.get(tableNames.size() - 1))) {
                sb.append(" union all ");
            }
        }
        sb.append(") as result");
//        System.out.println(sb.toString());

        String s = sb.toString();
        Statement statement = db.createStatement();
        ResultSet resultSet = statement.executeQuery(s);

        List<WeatherData> weatherData = new ArrayList<>();
        while (resultSet.next()) {
            WeatherData w = WeatherData.builder()
                    .station(stationId)
                    .min(resultSet.getInt(1))
                    .max(resultSet.getInt(2))
                    .avg(resultSet.getInt(3))
                    .build();

            weatherData.add(w);
        }

        return weatherData;
    }
    public static List<WeatherData> getWeatherDataByYear(int year) throws SQLException {
        List<String> tableNames = getTableNameByYear(year);

        StringBuilder sb = new StringBuilder();
        sb.append("select min(result.min),max(result.max),avg(result.avg) from (");
        for (String tableName : tableNames) {
            sb.append("select min(Temperatuur) as min,max(Temperatuur) as max,avg(Temperatuur) as avg from [");
            sb.append(tableName);
            sb.append("]");
            if (!tableName.equals(tableNames.get(tableNames.size() - 1))) {
                sb.append(" union all ");
            }
        }
        sb.append(") as result");
//        System.out.println(sb.toString());

        String s = sb.toString();
        Statement statement = db.createStatement();
        ResultSet resultSet = statement.executeQuery(s);

        List<WeatherData> weatherData = new ArrayList<>();
        while (resultSet.next()) {
            WeatherData w = WeatherData.builder()
                    .jaar(year)
                    .min(resultSet.getInt(1))
                    .max(resultSet.getInt(2))
                    .avg(resultSet.getInt(3))
                    .build();

            weatherData.add(w);
        }

        return weatherData;
    }

    private static List<String> getTableNameByStation(int stationId) throws SQLException {
        Connection db = DatabaseConnection.getInstance();
        String query = "SELECT TABLE_NAME FROM KNMI.INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' and TABLE_NAME like '" + stationId + "-%'";
        Statement statement = db.createStatement();
        ResultSet resultSet = statement.executeQuery(query);

        List<String> tables = new ArrayList<>();
        while (resultSet.next()) {
            tables.add(resultSet.getString(1));
        }
        return tables;
    }
    private static List<String> getTableNameByYear(int year) throws SQLException {
        Connection db = DatabaseConnection.getInstance();
        String query = "SELECT TABLE_NAME FROM KNMI.INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' and TABLE_NAME like '%-"+year+"'";
        Statement statement = db.createStatement();
        ResultSet resultSet = statement.executeQuery(query);

        List<String> tables = new ArrayList<>();
        while (resultSet.next()) {
            tables.add(resultSet.getString(1));
        }
        return tables;
    }
}
